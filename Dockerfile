FROM openjdk:8-jdk-alpine
COPY ./target/countrycode-0.0.1-SNAPSHOT.jar /user/app/
WORKDIR /user/app/
ENTRYPOINT ["java","-jar","countrycode-0.0.1-SNAPSHOT.jar"]