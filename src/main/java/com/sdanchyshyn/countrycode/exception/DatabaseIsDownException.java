package com.sdanchyshyn.countrycode.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class DatabaseIsDownException extends RuntimeException {
    public DatabaseIsDownException(String message){
        super(message);
    }
}
