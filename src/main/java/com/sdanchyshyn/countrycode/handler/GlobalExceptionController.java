package com.sdanchyshyn.countrycode.handler;

import com.sdanchyshyn.countrycode.exception.CountryCodeNotFoundException;
import com.sdanchyshyn.countrycode.exception.DatabaseIsDownException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionController {

    @ExceptionHandler(value = CountryCodeNotFoundException.class)
    public ResponseEntity<Object> exception(CountryCodeNotFoundException exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = DatabaseIsDownException.class)
    public ResponseEntity<Object> exception(DatabaseIsDownException exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
