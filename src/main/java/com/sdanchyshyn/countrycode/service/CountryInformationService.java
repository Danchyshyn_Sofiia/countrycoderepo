package com.sdanchyshyn.countrycode.service;

import com.sdanchyshyn.countrycode.exception.CountryCodeNotFoundException;
import com.sdanchyshyn.countrycode.exception.DatabaseIsDownException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CountryInformationService {

    @PersistenceContext
    private EntityManager entityManager;

    public Map<String, String> getCountryInfo(String countryCode) {

        String sqlQuery = "SELECT name, continent, population,life_expectancy, language FROM country " +
                "join country_language on country.code = country_language.country_code where code=:countryCode";

        Map<String, String> mapCountryInfo = new HashMap<>();
        try {
            Query query = entityManager.createNativeQuery(sqlQuery);
            query.setParameter("countryCode", countryCode);

            List<Object[]> resultList = query.getResultList();

            if (resultList.isEmpty()) {
                throw new CountryCodeNotFoundException("INVALID COUNTRY CODE");
            }

            for (Object[] objects : resultList) {
                mapCountryInfo.put("name", objects[0].toString());
                mapCountryInfo.put("continent", objects[1].toString());
                mapCountryInfo.put("population", objects[2].toString());
                mapCountryInfo.put("life_expectancy", objects[3].toString());
                mapCountryInfo.put("country_language", objects[4].toString());
            }

        } catch (PersistenceException e) {
            throw new DatabaseIsDownException("INTERNAL ERROR");
        }
        return mapCountryInfo;
    }
}
