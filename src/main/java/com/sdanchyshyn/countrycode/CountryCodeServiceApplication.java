package com.sdanchyshyn.countrycode;

import com.sdanchyshyn.countrycode.service.CountryInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@SpringBootApplication
@RestController
public class CountryCodeServiceApplication {

    @Autowired
    private CountryInformationService countryInformationService;

    @RequestMapping(value = "/{countryCode}")
    public Map<String, String> getCountryInfo(@PathVariable String countryCode) {
        return countryInformationService.getCountryInfo(countryCode);
    }

    public static void main(String[] args) {
        SpringApplication.run(CountryCodeServiceApplication.class, args);
    }

}
