package com.sdanchyshyn.countrycode;

import com.sdanchyshyn.countrycode.exception.CountryCodeNotFoundException;
import com.sdanchyshyn.countrycode.exception.DatabaseIsDownException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class CountryCodeServiceApplicationTests {

    @Autowired
    private CountryCodeServiceApplication countryCodeServiceApplication;

    @Test
    void getCountryInfo_Expected_INVALID_COUNTRY_CODE() {
        Exception exception = assertThrows(CountryCodeNotFoundException.class, () -> {
            countryCodeServiceApplication.getCountryInfo("TEST");
        });

        String expectedMessage = "INVALID COUNTRY CODE";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void getCountryInfo_Expected_INTERNAL_ERROR() {
        Exception exception = assertThrows(DatabaseIsDownException.class, () -> {
            countryCodeServiceApplication.getCountryInfo("BHR");
        });

        String expectedMessage = "INTERNAL ERROR";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

}
